package com.haud.gateway.smsgateway.excpetion;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	private final Logger LOGGER = LogManager.getLogger(getClass());

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<ErrorResponse> handleUnknownSystemException(WebRequest request, Exception e) {
		Throwable cause = e.getCause();
		String message = e.getMessage();
		this.LOGGER.error("handleUnknownSystemException().." + message, e);
		String errorCode = HttpStatus.INTERNAL_SERVER_ERROR.name();
		String errorMessage = "server side error";

		if (cause != null)
			message = message + ", " + cause.getMessage();
		ErrorResponse errorResponse = new ErrorResponse(errorCode, Integer.valueOf(1),
				(message == null) ? errorMessage : message);
		return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler({ IncorrectDestinationAddressException.class })
	public ResponseEntity<ErrorResponse> handleIncorrectDestinationAddressException(WebRequest request,
			IncorrectDestinationAddressException e) {
		this.LOGGER.error("Incorrect destination address.." + e.getMessage());
		String errorCode = HttpStatus.BAD_REQUEST.name();
		String errorMessage = e.getMessage();
		ErrorResponse errorResponse = new ErrorResponse(errorCode, Integer.valueOf(1), errorMessage);
		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}
}

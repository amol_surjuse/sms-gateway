package com.haud.gateway.smsgateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.haud.sms.model.Message;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "/sms")
@Slf4j
public class MessageController {
	@Autowired
	private KafkaTemplate<String, Message> kafkaTemplate;

	@PostMapping
	public void sendSms(@RequestBody Message message) {
		log.info("sms recived ");
		log.trace("sms recived SMS : {}", message);
		kafkaTemplate.send("sms", message);
	}
}

package com.haud.gateway.smsgateway.excpetion;

public class IncorrectDestinationAddressException extends Exception  {

  private static final long serialVersionUID = 1L;

  public IncorrectDestinationAddressException(String errorMsg) {
    super(errorMsg);
  }

}
